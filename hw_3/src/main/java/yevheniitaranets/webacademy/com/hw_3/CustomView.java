package yevheniitaranets.webacademy.com.hw_3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Yevhenii on 02.09.16.
 */
public class CustomView extends View {


    private static final String TAG = CustomView.class.getSimpleName();
    private boolean isInverted = false;
    private boolean isCroped = false;
    float[] cmData = new float[]{
            1, 0, 0, 0, 0,
            0, -1, 0, 0, 255,
            0, 0, -1, 0, 255,
            0, 0, 0, 1, 0};

    private boolean isGradient = false;
    private Rect rect;
    private ColorMatrix mColorMatrix;
    private ColorFilter mColorFilter;
    private Bitmap mImage;
    private Bitmap mOvalBitmap;

    Paint mPaint;
    PorterDuffXfermode porterDstIn = new PorterDuffXfermode(PorterDuff.Mode.DST_IN);
//    PorterDuffXfermode porterDstOver = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
    PorterDuffXfermode porterSrcMultiply = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);

    public CustomView(Context context) {
        super(context);
        Log.d(TAG, "CustomImageView(1)");


    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "CustomImageView(2)");


        mImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.android_rain);
        mColorMatrix = new ColorMatrix(cmData);
        mColorFilter = new ColorMatrixColorFilter(mColorMatrix);


    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(TAG, "CustomImageView(3)");

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw()");
        canvas.save();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG); //todo без этого не работают checkboxes

        if (isInverted) {
            mPaint.setColorFilter(mColorFilter);
        }
        canvas.drawBitmap(mImage, null, rect, mPaint);
        if (isCroped) {
            mPaint.setXfermode(porterDstIn);
            canvas.drawBitmap(mOvalBitmap, null, rect, mPaint);
        }
        if (isGradient) {
             mPaint.setXfermode(porterSrcMultiply);

            mPaint.setShader(getShader());
            canvas.drawPaint(mPaint);
        }
        canvas.restore();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(TAG, "onMeasure() w=" + widthMeasureSpec + " h=" + heightMeasureSpec);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG, "onSizeChanged() w=" + w + " h=" + h);
        rect = new Rect(0, 0, w, h);
        mOvalBitmap = createCircle(w, h);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d(TAG, "onLayout()");

    }


    public void setInverted(boolean inverted) {
        isInverted = inverted;
        postInvalidate();
    }

    public void setCroped(boolean croped) {
        isCroped = croped;
        postInvalidate();

    }

    public void setGradient(boolean gradient) {
        isGradient = gradient;
        postInvalidate();

    }
    public static Bitmap createCircle(int width, int height) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        float radius = Math.min(width, height) * 0.45f;
        canvas.drawCircle(width / 2, height / 2, radius, paint);

        return bitmap;
    }

    public Shader getShader() {
        int[] rainbow = getRainbowColors();
        Shader shader = new LinearGradient(0, 0, 0, 500, rainbow,
                null, Shader.TileMode.MIRROR);

        Matrix matrix = new Matrix();
        matrix.setRotate(90);
        shader.setLocalMatrix(matrix);
        return shader;
    }

    private int[] getRainbowColors() {
        return new int[]{
                ContextCompat.getColor(getContext(), R.color.rainbow_red),
                ContextCompat.getColor(getContext(), R.color.rainbow_yellow),
                ContextCompat.getColor(getContext(), R.color.rainbow_green),
                ContextCompat.getColor(getContext(), R.color.rainbow_blue),
                ContextCompat.getColor(getContext(), R.color.rainbow_purple)
        };
    }


}
