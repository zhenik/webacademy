package yevheniitaranets.webacademy.com.hw_3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private CustomView customView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CheckBox chbInvert = (CheckBox) findViewById(R.id.chbInvert);
        CheckBox chbCrop = (CheckBox) findViewById(R.id.chbCrop);
        CheckBox chbGradient = (CheckBox) findViewById(R.id.chbGradient);
        chbInvert.setOnCheckedChangeListener(this);
        chbCrop.setOnCheckedChangeListener(this);
        chbGradient.setOnCheckedChangeListener(this);

        customView = (CustomView) findViewById(R.id.myView);


    }


    @Override
    public void onCheckedChanged(CompoundButton view, boolean b) {

        switch (view.getId()) {
            case R.id.chbInvert:
                customView.setInverted(b);
                break;
            case R.id.chbCrop:
                customView.setCroped(b);

                break;
            case R.id.chbGradient:
                customView.setGradient(b);
                break;
        }
    }


}
