package yevheniitaranets.webacademy.com.hw_2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String TAG_GREEN = "green";
    private static final String TAG_BLUE = "blue";
    private static final String TAG_RED = "red";

    private CheckBox checkBoxR;
    private CheckBox checkBoxG;
    private CheckBox checkBoxB;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkBoxR = (CheckBox) findViewById(R.id.checkBoxR);
        checkBoxG = (CheckBox) findViewById(R.id.checkBoxG);
        checkBoxB = (CheckBox) findViewById(R.id.checkBoxB);
        checkBoxR.setOnCheckedChangeListener(this);
        checkBoxG.setOnCheckedChangeListener(this);
        checkBoxB.setOnCheckedChangeListener(this);
        fragmentManager = getSupportFragmentManager();
    }


    @Override
    public void onCheckedChanged(CompoundButton view, boolean b) {

        switch (view.getId()) {
            case R.id.checkBoxR:
                actionDependsOnCheckedAndTag(b, TAG_RED);
                break;
            case R.id.checkBoxG:
                actionDependsOnCheckedAndTag(b, TAG_GREEN);
                break;
            case R.id.checkBoxB:
                actionDependsOnCheckedAndTag(b, TAG_BLUE);
                break;
        }
    }

    void showFragmentWithTag(String tag) {
        Fragment fragment = new Fragment();
        switch (tag) {
            case TAG_GREEN:
                fragment = new FragmentG();
                break;
            case TAG_BLUE:
                fragment = new FragmentB();
                break;
            case TAG_RED:
                fragment = new FragmentR();
                break;
        }
        fragmentManager.beginTransaction()
                .add(R.id.flMainContainer, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    void actionDependsOnCheckedAndTag(boolean checked, String tag) {
        if (checked) {
            Log.d(TAG, "add Fragment " + tag);
            showFragmentWithTag(tag);
        } else {
            Fragment f = fragmentManager.findFragmentByTag(tag);
            if (f != null) {
                Log.d(TAG, "onCheckedFalse " + f.getClass().getSimpleName());
                fragmentManager.beginTransaction().remove(f).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = fragmentManager.findFragmentById(R.id.flMainContainer);
        if (f != null) {
            Log.d(TAG, "onBackPressed " + f.getClass().getSimpleName());
            if (f instanceof FragmentR) {
                checkBoxR.setChecked(false);
            } else if (f instanceof FragmentB) {
                checkBoxB.setChecked(false);
            } else if (f instanceof FragmentG) {
                checkBoxG.setChecked(false);
            }

            fragmentManager.popBackStack();
        } else finish();//todo finish backstack
    }
}
