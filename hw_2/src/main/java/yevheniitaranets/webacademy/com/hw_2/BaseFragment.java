package yevheniitaranets.webacademy.com.hw_2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Yevhenii on 18.08.16.
 */
public class BaseFragment extends Fragment {

    private FrameLayout flBackground;

    public void setBackgroundColor(int backgroundColor) {
        flBackground.setBackgroundColor(backgroundColor);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.base_fragment_layout, container, false);
        flBackground = (FrameLayout) v.findViewById(R.id.flBackground);
        return v;
    }


}
